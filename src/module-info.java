module DEV3 {
    requires javafx.fxml;
    requires javafx.controls;
    requires com.jfoenix;

    exports pbaface.Controller;
    exports pbaface.Main;


    opens pbaface.Controller;
    opens pbaface.Main;
    opens pbaface.FXML;
}