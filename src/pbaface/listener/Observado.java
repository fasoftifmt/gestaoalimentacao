package pbaface.listener;

import pbaface.Model.DadosAluno;

public interface Observado {

    void registrar(Observador observador);
    void notificar();

}
