package pbaface.listener;

import pbaface.Model.DadosAluno;

public interface Observador {
    void atualizar(DadosAluno dados);
}
