package pbaface.Controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pbaface.Main.HistoryMain;
import pbaface.Main.InformationMain;

import java.net.URL;
import java.util.ResourceBundle;

public class HistoryController implements Initializable {

    @FXML
    private Button btnSair;

    @FXML
    private TableColumn column1;

    @FXML
    private TableColumn column2;

    @FXML
    private TableColumn column3;

    @FXML
    private TableColumn column4;

    @FXML
    private TableView table;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnSair.setOnMouseClicked((MouseEvent e)->{
            fecha();
        });
    }

    private void fecha(){
        HistoryMain.getStage().close();
    }
}
