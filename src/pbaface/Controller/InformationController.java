package pbaface.Controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pbaface.Main.CadastroMain;
import pbaface.Main.HistoryMain;
import pbaface.Main.InformationMain;
import pbaface.Main.OptionsMain;
import pbaface.Model.DadosAluno;
import pbaface.listener.Observador;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class InformationController implements Initializable, Observador {
    @FXML
    private Pane imgPhoto;

    @FXML
    private ImageView imgGif;

    @FXML
    private Label lblNome;

    @FXML
    private Label lblCurso;

    @FXML
    private Label lblMatricula;

    @FXML
    private ImageView imglogo;

    @FXML
    private Button btnVoltar;

    @FXML
    private TextField txtvalida;
    @FXML
    private Button hist;

    @FXML
    private Button cadastro;

    private Map<String, DadosAluno> mapa = new HashMap<>();

    @Override
    public void initialize(URL url, ResourceBundle rb){
        recuperarDados();

        cadastro.setOnMouseClicked((MouseEvent e)->{
            CadastroMain cad = new CadastroMain();
            try {
                cad.start(new Stage());
                cad.getController().registrar(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        btnVoltar.setOnMouseClicked((MouseEvent e)->    {
            OptionsMain op = new OptionsMain();
            try {
                op.start(new Stage());
                fecha();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        FileInputStream input = null;
        try {
            input = new FileInputStream("C:\\DEV4\\gestaoalimentacao\\Images\\octayde.png");
            Image image = new Image(input);
            imglogo.setImage(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        txtvalida.textProperty().addListener((observable, oldValue, newValue) -> {

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

           if(isNumeroCartaoValido(newValue)){
               System.out.println(newValue);
               txtvalidaOnAction(newValue);
               updateGif("C:\\DEV4\\gestaoalimentacao\\Images\\check.gif");
           }else{
               updateGif("C:\\DEV4\\gestaoalimentacao\\Images\\x.gif");
           }
        });

        lblNome.setVisible(false);
        lblMatricula.setVisible(false);
        lblCurso.setVisible(false);

        hist.setOnMouseClicked(e ->{
            HistoryMain hist = new HistoryMain();
            try {
                hist.start(new Stage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    private void updateGif(String caminho) {
        FileInputStream input;
        try {
            input = new FileInputStream(caminho);
            Image image = new Image(input);
            imgGif.setImage(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void txtvalidaOnAction(String valor){
        DadosAluno dadosAluno = mapa.get(valor);
        setLabels(dadosAluno);
    }

    private void setLabels(DadosAluno dadosAluno) {
        if (dadosAluno == null) throw new IllegalStateException("Dados do aluno está nulo");
        lblNome.setVisible(true);
        lblMatricula.setVisible(true);
        lblCurso.setVisible(true);
        System.out.println(dadosAluno);
        lblNome.setText(dadosAluno.getNome());
        lblCurso.setText(dadosAluno.getCurso());
        lblMatricula.setText(dadosAluno.getMatricula());
    }

    private void recuperarDados(){
        try(var reader = new BufferedReader(new FileReader("C:\\DEV4\\gestaoalimentacao\\Files\\Info.csv"))){
           while(reader.ready()) {
               String s = reader.readLine();
               String[] split = s.split(",");
               var dadosAluno = new DadosAluno();
               dadosAluno.setNumeroCartao(split[0]);
               dadosAluno.setNome(split[1]);
               dadosAluno.setCurso(split[2]);
               dadosAluno.setMatricula(split[3]);
               mapa.put(dadosAluno.getNumeroCartao(), dadosAluno);
           }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void fecha() {
        InformationMain.getStage().close();
    }

    public boolean isNumeroCartaoValido(String numeroLido){
        return mapa.containsKey(numeroLido);
    }

    @Override
    public void atualizar(DadosAluno dados) {
        setLabels(dados);
    }
}
