package pbaface.Controller;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import pbaface.Main.InformationMain;
import pbaface.Main.LoginMain;
import pbaface.Main.OptionsMain;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;

public class OptionsController implements Initializable {


    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private ImageView imglogo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btn1.setOnMouseClicked((MouseEvent e) -> {
            InformationMain info = new InformationMain();
            try {
                info.start(new Stage());
                fecha();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        btn2.setOnMouseClicked((MouseEvent e) -> {
            System.out.println("Alimentação clicado");
            InformationMain info = new InformationMain();
            try {
                info.start(new Stage());
                fecha();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        btn3.setOnMouseClicked((MouseEvent e) -> {
            LoginMain login = new LoginMain();
            try {
                login.start(new Stage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            fecha();
        });

        btn3.setOnKeyPressed((KeyEvent e)->{
            if(e.getCode() == KeyCode.ENTER){
                fecha();
            }
        });

        FileInputStream input = null;
        try {
            input = new FileInputStream("C:\\DEV4\\gestaoalimentacao\\Images\\octayde.png");
            Image image = new Image(input);
            this.imglogo.setImage(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    public void fecha() {
        OptionsMain.getStage().close();
    }

}

