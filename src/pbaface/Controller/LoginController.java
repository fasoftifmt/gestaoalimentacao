package pbaface.Controller;

import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pbaface.Main.HistoryMain;
import pbaface.Main.OptionsMain;
import pbaface.Main.LoginMain;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    private ImageView imglogo;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnSair;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private JFXTextField user;
    @FXML
    private JFXPasswordField pass;
    @FXML
    private StackPane myStackPane;



    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnSair.setOnMouseClicked((MouseEvent e)->{
            fecha();
        });
        btnLogin.setOnMouseClicked((MouseEvent e )->{
          login();
        });
//        btnLogin.setOnKeyPressed(e->{
//            if(e.getCode() == KeyCode.ENTER){
//                login();
//                System.out.println();
//            }
//        });
        pass.setOnKeyPressed(e ->{
            if(e.getCode() == KeyCode.ENTER){
                login();
            }
        });

        images();

    }

    public void images(){
        FileInputStream input = null;

        try {
            input = new FileInputStream("C:\\DEV4\\gestaoalimentacao\\Images\\user.png");
            Image image = new Image(input);
            this.img1.setImage(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            input = new FileInputStream("C:\\DEV4\\gestaoalimentacao\\Images\\lock.png");
            Image image = new Image(input);
            this.img2.setImage(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        try {
            input = new FileInputStream("C:\\DEV4\\gestaoalimentacao\\Images\\octayde.png");
            Image image = new Image(input);
            this.imglogo.setImage(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void fecha(){

        LoginMain.getStage().close();
    }

    public void login(){
        if(user.getText().equals("root") && pass.getText().equals("1234")){
            OptionsMain op = new OptionsMain();
            try {
                op.start(new Stage());
                fecha();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else{
           negado();
        }

    }

    public void negado(){
        String title = "Senha Inválida";
        String error = "A senha digitada está incorreta, por favor insira a senha novamente";
        JFXDialogLayout dialog = new JFXDialogLayout();
        dialog.setHeading(new Text(title));
        dialog.setBody(new Text(error));
        JFXButton button = new JFXButton("Voltar");
        button.setButtonType(JFXButton.ButtonType.RAISED);
        button.setStyle("-fx-background-color:#2f9e41;");
        dialog.setActions(button);

        JFXDialog dialog1 = new JFXDialog(myStackPane, dialog, JFXDialog.DialogTransition.BOTTOM);

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog1.close();
            }
        });
        dialog1.show();
    }

}
