package pbaface.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pbaface.Main.CadastroMain;
import pbaface.Model.DadosAluno;
import pbaface.listener.Observado;
import pbaface.listener.Observador;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class CadastroController implements Initializable, Observado {

    @FXML
    private Button btnValidar;

    @FXML
    private Button btnVoltar;

    @FXML
    private StackPane myStackPane;

    @FXML
    private TextField matricula;

    private DadosAluno dadosAluno;
    private Observador observador;

    public DadosAluno getDadosAluno() {
        return dadosAluno;
    }

    public void setDadosAluno(DadosAluno dadosAluno) {
        this.dadosAluno = dadosAluno;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.dadosAluno = new DadosAluno();

        btnVoltar.setOnMouseClicked((MouseEvent e)->{
            fecha();
        });
        btnValidar.setOnMouseClicked((MouseEvent e)->{
            validarMatricula();
        });

        matricula.setOnKeyPressed(e ->{
            if(e.getCode() == KeyCode.ENTER){
                validarMatricula();
            }
        });
    }

    public void validarMatricula(){
        try(var reader = new BufferedReader(new FileReader("C:\\DEV4\\gestaoalimentacao\\Files\\Info.csv"))){

            boolean isValid = false;

            while(reader.ready()) {
                String s = reader.readLine();
                String[] split = s.split(",");
                if(matricula.getText().equals(split[3])){
                    dadosAluno.setNumeroCartao(split[0]);
                    dadosAluno.setNome(split[1]);
                    dadosAluno.setCurso(split[2]);
                    dadosAluno.setMatricula(split[3]);

                    isValid = true;
                    break;
                }
            }
            if(isValid){
                try{
                    fecha();
                    System.out.println("Matricula Cadastrada");
                    notificar();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }else{
                matriculaIncorreta();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void matriculaIncorreta(){
        String title = "Matrícula Inválida";
        String error = "A matrícula informada está incorreta, por favor insira novamente!!";
        JFXDialogLayout dialog = new JFXDialogLayout();
        dialog.setHeading(new Text(title));
        dialog.setBody(new Text(error));
        JFXButton button = new JFXButton("Voltar");
        button.setButtonType(JFXButton.ButtonType.RAISED);
        button.setStyle("-fx-background-color:#2f9e41;");
        dialog.setActions(button);

        JFXDialog dialog1 = new JFXDialog(myStackPane, dialog, JFXDialog.DialogTransition.BOTTOM);

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog1.close();
            }
        });
        dialog1.show();
    }

    private void fecha(){
        CadastroMain.getStage().close();
    }

    @Override
    public void registrar(Observador observador) {
        this.observador = observador;
    }

    @Override
    public void notificar() {
        this.observador.atualizar(dadosAluno);
    }
}