package pbaface.Model;

import java.util.Objects;

public class DadosAluno {

    private String numeroCartao;
    private String nome;
    private String matricula;
    private String curso;

    public DadosAluno(String numeroCartao, String nome, String matricula, String curso) {
        this.numeroCartao = numeroCartao;
        this.nome = nome;
        this.matricula = matricula;
        this.curso = curso;
    }

    public DadosAluno() {

    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DadosAluno that = (DadosAluno) o;
        return numeroCartao.equals(that.numeroCartao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numeroCartao);
    }

    @Override
    public String toString() {
        return "DadosAluno{" +
                "numeroCartao='" + numeroCartao + '\'' +
                ", nome='" + nome + '\'' +
                ", matricula='" + matricula + '\'' +
                ", curso='" + curso + '\'' +
                '}';
    }
}
