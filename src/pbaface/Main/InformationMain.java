package pbaface.Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class InformationMain extends Application {
    private static Stage stage;


    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        InformationMain.stage = stage;
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/pbaface/FXML/FXMLInformations.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Informações do Aluno");
        stage.setScene(scene);
        stage.show();
        setStage(stage);
    }
}
