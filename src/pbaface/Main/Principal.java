package pbaface.Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Principal extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/pbaface/FXML/FXMLOptions.fxml"));
        Parent root = loader.load();
        stage.setScene(new Scene(root, 600, 405));
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
