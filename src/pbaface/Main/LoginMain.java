package pbaface.Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LoginMain extends Application {
    private static Stage stage;

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        LoginMain.stage = stage;
    }


    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/pbaface/FXML/FXMLLogin.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Login de Usuario");
        stage.setScene(scene);
        stage.show();
        setStage(stage);
    }
}
