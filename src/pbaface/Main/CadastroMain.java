package pbaface.Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pbaface.Controller.CadastroController;

public class CadastroMain extends Application {
    private static Stage stage;

    private CadastroController controller;

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        CadastroMain.stage = stage;
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/pbaface/FXML/FXMLCadastro.fxml"));
        Parent root = loader.load();
        setController(loader.getController());
        setStage(stage);
        Scene scene = new Scene(root);
        stage.setTitle("Registro Manual do Aluno");
        stage.setScene(scene);
        stage.show();
    }

    public CadastroController getController() {
        return controller;
    }

    public void setController(CadastroController controller) {
        this.controller = controller;
    }
}
